/*
 Navicat Premium Data Transfer

 Source Server         : localhost-sqlserver
 Source Server Type    : SQL Server
 Source Server Version : 13005026
 Source Host           : localhost:1433
 Source Catalog        : test
 Source Schema         : dbo

 Target Server Type    : SQL Server
 Target Server Version : 13005026
 File Encoding         : 65001

 Date: 24/03/2022 15:34:45
*/


-- ----------------------------
-- Table structure for user
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[user]') AND type IN ('U'))
	DROP TABLE [dbo].[user]
GO

CREATE TABLE [dbo].[user] (
  [id] int  NOT NULL,
  [name] varchar(32) COLLATE Chinese_PRC_CI_AS DEFAULT NULL NULL,
  [user_no] varchar(64) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [create_time] datetime DEFAULT NULL NULL,
  [update_time] datetime DEFAULT NULL NULL,
  [version] int DEFAULT NULL NULL
)
GO

ALTER TABLE [dbo].[user] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO [dbo].[user] ([id], [name], [user_no], [create_time], [update_time], [version]) VALUES (N'14', N'testM1', N'a9d1a1c1-80d3-4393-8353-7268d8393ce5', N'2020-03-01 04:44:19.000', NULL, N'1')
GO

INSERT INTO [dbo].[user] ([id], [name], [user_no], [create_time], [update_time], [version]) VALUES (N'15', N'testS1', N'a1e79764-db62-460f-926e-fd8b61fb035e', N'2020-03-01 04:44:50.000', NULL, N'1')
GO

INSERT INTO [dbo].[user] ([id], [name], [user_no], [create_time], [update_time], [version]) VALUES (N'16', N'testM1', N'3868c51c-feb1-47ab-beea-d74ccd32e535', N'2020-03-01 04:46:05.000', NULL, N'1')
GO

INSERT INTO [dbo].[user] ([id], [name], [user_no], [create_time], [update_time], [version]) VALUES (N'17', N'testS1', N'0c7185e6-5077-4a0a-ac28-7472b10b1185', N'2020-03-01 04:46:16.000', NULL, N'1')
GO

INSERT INTO [dbo].[user] ([id], [name], [user_no], [create_time], [update_time], [version]) VALUES (N'18', N'testM1', N'820a0e05-2a02-4b57-9a59-ffab6e3c2d71', N'2020-03-02 15:05:27.000', NULL, N'1')
GO

INSERT INTO [dbo].[user] ([id], [name], [user_no], [create_time], [update_time], [version]) VALUES (N'19', N'testS1', N'0f52488e-377e-4573-8346-97afc1be231d', N'2020-03-02 15:05:44.000', NULL, N'1')
GO

INSERT INTO [dbo].[user] ([id], [name], [user_no], [create_time], [update_time], [version]) VALUES (N'22', N'testM1', N'b227db1b-25c2-4788-ae66-189dc728ad1c', N'2020-03-02 15:09:10.000', NULL, N'1')
GO

INSERT INTO [dbo].[user] ([id], [name], [user_no], [create_time], [update_time], [version]) VALUES (N'23', N'testS1', N'24a5f5de-951b-4ad3-81a5-9f28ff1deec8', N'2020-03-02 15:09:28.000', NULL, N'1')
GO

INSERT INTO [dbo].[user] ([id], [name], [user_no], [create_time], [update_time], [version]) VALUES (N'24', N'testM1', N'b49bdb20-eaae-4f35-8b08-aa01cebe034c', N'2020-03-02 15:10:31.000', NULL, N'1')
GO

INSERT INTO [dbo].[user] ([id], [name], [user_no], [create_time], [update_time], [version]) VALUES (N'25', N'testS1', N'fe3c7f7f-47b0-435a-b255-c383a637eefa', N'2020-03-02 15:10:38.000', NULL, N'1')
GO

INSERT INTO [dbo].[user] ([id], [name], [user_no], [create_time], [update_time], [version]) VALUES (N'37', N'testS1', N'dc0a845f-7068-4d34-b259-598c1e55da4f', N'2020-03-02 15:34:12.000', NULL, N'1')
GO


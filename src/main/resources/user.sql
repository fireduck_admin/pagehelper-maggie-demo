/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80017
 Source Host           : localhost:3306
 Source Schema         : test

 Target Server Type    : MySQL
 Target Server Version : 80017
 File Encoding         : 65001

 Date: 24/03/2022 06:49:22
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_no` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `version` int(11) NULL DEFAULT NULL COMMENT '版本号',
  PRIMARY KEY (`id`, `user_no`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 38 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (14, 'testM1', 'a9d1a1c1-80d3-4393-8353-7268d8393ce5', '2020-03-01 04:44:19', NULL, 1);
INSERT INTO `user` VALUES (15, 'testS1', 'a1e79764-db62-460f-926e-fd8b61fb035e', '2020-03-01 04:44:50', NULL, 1);
INSERT INTO `user` VALUES (16, 'testM1', '3868c51c-feb1-47ab-beea-d74ccd32e535', '2020-03-01 04:46:05', NULL, 1);
INSERT INTO `user` VALUES (17, 'testS1', '0c7185e6-5077-4a0a-ac28-7472b10b1185', '2020-03-01 04:46:16', NULL, 1);
INSERT INTO `user` VALUES (18, 'testM1', '820a0e05-2a02-4b57-9a59-ffab6e3c2d71', '2020-03-02 15:05:27', NULL, 1);
INSERT INTO `user` VALUES (19, 'testS1', '0f52488e-377e-4573-8346-97afc1be231d', '2020-03-02 15:05:44', NULL, 1);
INSERT INTO `user` VALUES (22, 'testM1', 'b227db1b-25c2-4788-ae66-189dc728ad1c', '2020-03-02 15:09:10', NULL, 1);
INSERT INTO `user` VALUES (23, 'testS1', '24a5f5de-951b-4ad3-81a5-9f28ff1deec8', '2020-03-02 15:09:28', NULL, 1);
INSERT INTO `user` VALUES (24, 'testM1', 'b49bdb20-eaae-4f35-8b08-aa01cebe034c', '2020-03-02 15:10:31', NULL, 1);
INSERT INTO `user` VALUES (25, 'testS1', 'fe3c7f7f-47b0-435a-b255-c383a637eefa', '2020-03-02 15:10:38', NULL, 1);
INSERT INTO `user` VALUES (37, 'testS1', 'dc0a845f-7068-4d34-b259-598c1e55da4f', '2020-03-02 15:34:12', NULL, 1);

SET FOREIGN_KEY_CHECKS = 1;

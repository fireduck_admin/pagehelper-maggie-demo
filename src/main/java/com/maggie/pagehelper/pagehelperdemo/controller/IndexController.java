package com.maggie.pagehelper.pagehelperdemo.controller;

import com.fasterxml.jackson.databind.util.JSONPObject;
import com.github.pagehelper.PageHelper;
import com.maggie.pagehelper.pagehelperdemo.dao.UserMapper;
import com.maggie.pagehelper.pagehelperdemo.dto.User;
import com.maggie.pagehelper.pagehelperdemo.dto.UserExample;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;


/**
 * 
 * @author Administrator
 *
 */
@Controller
@RequestMapping("/index")
public class IndexController {
    private final static Logger logger = LoggerFactory.getLogger(IndexController.class);

    @Autowired
    UserMapper userMapper;

    @RequestMapping(value = "ph")
    @ResponseBody
    // 使用了@RequestBody，不能在拦截器中，获得流中的数据，再json转换，拦截器中，也不清楚数据的类型，无法转换成java对象
    // 只能手动调用方法
    public Object ph(Integer page){
        PageHelper.startPage(page==null?0:page , 3);
        UserExample userExample = new UserExample();
        List<User> users = userMapper.selectByExample(userExample);
        return users;
    }
    @RequestMapping(value = "ph2")
    @ResponseBody
    // 使用了@RequestBody，不能在拦截器中，获得流中的数据，再json转换，拦截器中，也不清楚数据的类型，无法转换成java对象
    // 只能手动调用方法
    public Object ph2(Integer page){
        PageHelper.startPage(page==null?0:page , 3);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "success";
    }
    @RequestMapping(value = "ph3")
    @ResponseBody
    // 使用了@RequestBody，不能在拦截器中，获得流中的数据，再json转换，拦截器中，也不清楚数据的类型，无法转换成java对象
    // 只能手动调用方法
    public Object ph3(Integer page){
        // distinct 没问题
//        List<String> users = userMapper.selectDistinct(new User());
        // count
//        int users = userMapper.selectCount(new User());
//         sqlserver top
        List<String> users = userMapper.selectDistinctNoTop(new User());
        return users;
    }

}

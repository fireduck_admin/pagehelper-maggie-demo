package com.maggie.pagehelper.pagehelperdemo.dao;

import com.maggie.pagehelper.pagehelperdemo.dto.User;
import com.maggie.pagehelper.pagehelperdemo.dto.UserExample;
import com.maggie.pagehelper.pagehelperdemo.dto.UserKey;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface UserMapper {
    int countByExample(UserExample example);

    int deleteByExample(UserExample example);

    int deleteByPrimaryKey(UserKey key);

    int insert(User record);

    int insertSelective(User record);

    List<User> selectByExample(UserExample example);

    User selectByPrimaryKey(UserKey key);

    int updateByExampleSelective(@Param("record") User record, @Param("example") UserExample example);

    int updateByExample(@Param("record") User record, @Param("example") UserExample example);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    List<String> selectDistinct(User record);

    int selectCount(User record);
    
    List<String> selectDistinctTop(User user);
    
    List<String> selectDistinctNoTop(User user);
}